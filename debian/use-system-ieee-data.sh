#!/bin/sh

set -e

if [ $# -eq 0 ]; then
    echo "Usage: $0 <regenerate|restore>"
    exit 1
fi

for dir in databases/ieee-*; do
    database=${dir#*-}
    case "$database" in
        oui28)
            ieee_csv="mam.csv"
            ;;
        *)
            ieee_csv="${database}.csv"
            ;;
    esac
    (
        cd "$dir"
        if [ "$1" = "regenerate" ]; then
            echo "Regenerating ${dir}"
            if [ ! -f "/var/lib/ieee-data/${ieee_csv}" ]; then
                echo "Skipping database ${database} as a CSV for it doesn't exist."
                exit
            fi
            if [ ! -f "dbieee_${database}.h.orig" ]; then
                # keep a copy of the original "source" file
                # but only if we haven't done so already to make this idempotent
                mv "dbieee_${database}.h" "dbieee_${database}.h.orig"
            fi
            cp "/var/lib/ieee-data/${ieee_csv}" .
            make create
        elif [ "$1" = "restore" ]; then
            echo "Restoring ${dir}"
            if [ -f "dbieee_${database}.h.orig" ]; then
                mv "dbieee_${database}.h.orig" "dbieee_${database}.h"
            fi
            rm -f "${ieee_csv}"
        fi
    )
done
